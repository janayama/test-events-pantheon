pragma solidity ^0.5.0;

contract TestingEvents {
    event MyEvent(address sender);

    function emitEvent() external 
    {
        emit MyEvent(msg.sender);
    }
}
