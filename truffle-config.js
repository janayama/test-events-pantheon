const PrivateKeyProvider = require("truffle-hdwallet-provider");
const privateKey = "c87509a1c067bbde78beb793e6fa76530b6382a4c0241e5e4a9ec0a0f44dc0d2";
const pantheonProvider = new PrivateKeyProvider(privateKey, "https://regular.pre.iosec.io.builders:8565");

module.exports = {
  plugins: ["truffle-security"],

  networks: {
    pantheon: {
      host: "https://regular.pre.iosec.io.builders",
      port: 8565,
      network_id: "*", // Match any network id
      provider: pantheonProvider,
      gas: "0x1f87765a2aa7",
      gasPrice: 0x0,
    }
  },
  compilers: {
    solc: {
      version: '0.5.5',
      docker: false,
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        },
        emVersion: "byzantium"
      }
    }
  },
  mocha: {
    useColors: true,
    enableTimeouts: false
  },
  plugins: ["truffle-security"]
};
