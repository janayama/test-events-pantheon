const TestingEvents = artifacts.require('./TestingEvents.sol');

module.exports = async function () {
  try {
    const contract = await TestingEvents.new();
    console.log("created contract: " + contract.address);

    await contract.emitEvent();

    const contract1 = await TestingEvents.new();
    console.log("created contract1: " + contract1.address);
    const contract2 = await TestingEvents.new();
    console.log("created contract2: " + contract2.address);
    const contract3 = await TestingEvents.new();
    console.log("created contract3: " + contract3.address);
    
    console.log("emitEvent1");
    await contract1.emitEvent();
    console.log("emitEvent2");
    await contract2.emitEvent();
    console.log("emitEvent3");
    await contract3.emitEvent();
  } catch (e) {
    console.log(e);
  }
}
